package za.co.pnpstaff.server.error;


public class ErrorMessage {

    private String code;
    private String message;
    private String exceptionTrace;

    public ErrorMessage() {

    }

    public ErrorMessage(String code, String message, String exceptionTrace) {
        this.code = code;
        this.message = message;
        this.exceptionTrace = exceptionTrace;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getExceptionTrace() {
        return exceptionTrace;
    }

    public void setExceptionTrace(String exceptionTrace) {
        this.exceptionTrace = exceptionTrace;
    }
}
