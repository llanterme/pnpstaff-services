package za.co.pnpstaff.server.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import za.co.pnpstaff.core.service.interfaces.BasketService;
import za.co.pnpstaff.server.error.NewExceptionHandlingUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Component
@Path("/basket")
public class BasketResource extends AbstractResource {

    @Autowired
    BasketService basketService;

    @GET
    public Response getBasketDetails(@QueryParam("recieptNumber") String recieptNumber) {

        try {
            return Response.ok().entity(basketService.getBasket(recieptNumber)).build();
        } catch (Exception e) {
            throw NewExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, BasketResource.class, "getBasketDetails", null);
        }
    }
}


