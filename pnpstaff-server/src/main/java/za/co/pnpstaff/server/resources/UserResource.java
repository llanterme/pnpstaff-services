package za.co.pnpstaff.server.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.pnpstaff.core.domain.User;
import za.co.pnpstaff.core.service.interfaces.LoyaltyService;
import za.co.pnpstaff.core.service.interfaces.UserService;
import za.co.pnpstaff.server.error.NewExceptionHandlingUtil;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Component
@Path("/user")
public class UserResource extends AbstractResource {

    @Autowired
    UserService userService;

    @POST
    @Path("/add")
    public Response addUser(User user) {

        try {
            return Response.ok().entity(userService.addUser(user)).build();
        } catch (Exception e) {
            throw NewExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, UserResource.class, "addLoyalty", null);
        }
    }
}

