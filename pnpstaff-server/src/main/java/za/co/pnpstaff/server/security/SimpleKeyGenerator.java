package za.co.pnpstaff.server.security;

/**
 * Created by Luke on 3/22/17.
 */
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

@Service
public class SimpleKeyGenerator implements KeyGenerator {


    // ======================================

    @Override
    public Key generateKey() {
        String keyString = "simplekey";
        Key key = new SecretKeySpec(keyString.getBytes(), 0, keyString.getBytes().length, "DES");
        return key;
    }
}
