package za.co.pnpstaff.server.error;


import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class CustomWebApplicationException extends WebApplicationException {

    private static final long serialVersionUID = 797591873437221010L;

    private String code;

    public CustomWebApplicationException(int status, ErrorMessage errorMessage, Throwable cause) {
        super(cause, Response.status(status).entity(errorMessage).build());
        this.code = errorMessage != null ? errorMessage.getCode() : null;
    }

    /**
     * @param status May not be null
     * @param errorMessage
     * @param cause
     */
    public CustomWebApplicationException(Response.Status status, ErrorMessage errorMessage, Throwable cause) {
        this(status.getStatusCode(), errorMessage, cause);
    }

    /**
     * @param status May not be null
     * @param code
     * @param message
     */
    public CustomWebApplicationException(Response.Status status, String code, String message) {
        this(status.getStatusCode(), code, message);
    }

    /**
     * @param status May not be null
     * @param errorMessage
     */
    public CustomWebApplicationException(Response.Status status, ErrorMessage errorMessage) {
        this(status, errorMessage, null);
    }

    public CustomWebApplicationException(int status, String code, String message) {
        this(status, new ErrorMessage(code, message, null), null);
    }

    public String getCode() {
        return code;
    }

}
