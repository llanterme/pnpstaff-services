package za.co.pnpstaff.server.resources;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.pnpstaff.core.service.interfaces.ApiService;
import za.co.pnpstaff.server.error.NewExceptionHandlingUtil;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

@Component
@Path("/login")
public class LoginResource extends AbstractResource {

    @Autowired
    ApiService apiService;

    @POST
    public Response authenticateUser() {

        try {
            //Get Authorization Header
            String authString = headers.getRequestHeader("Authorization").get(0);


            String basicAuthEncoded = authString.substring(6);
            String basicAuthString = new String(Base64.decodeBase64(basicAuthEncoded.getBytes()));
            String[] usernameAndPassword = basicAuthString.split(":");


            // Issue a token for the user
            String token = issueToken(basicAuthString);

            // Return the token on the response
            return Response.ok().header(AUTHORIZATION, "Bearer " + token).build();
        } catch (Exception e) {
                throw NewExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, ApiInfoResource.class, "version", null);
        }
    }


    private String issueToken(String login) {
        Key key = keyGenerator.generateKey();
        String jwtToken = Jwts.builder()
                .setSubject(login)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(toDate(LocalDateTime.now().plusMinutes(15L)))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
        //logger.info("#### generating token for a key : " + jwtToken + " - " + key);
        return jwtToken;

    }

    private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}

