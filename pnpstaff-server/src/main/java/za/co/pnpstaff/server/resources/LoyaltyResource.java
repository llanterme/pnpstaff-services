package za.co.pnpstaff.server.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.pnpstaff.core.ApplicationConfig;
import za.co.pnpstaff.core.domain.Loyalty;
import za.co.pnpstaff.core.service.interfaces.BasketService;
import za.co.pnpstaff.core.service.interfaces.CoffeeStampService;
import za.co.pnpstaff.core.service.interfaces.LoyaltyService;
import za.co.pnpstaff.server.error.NewExceptionHandlingUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Component
@Path("/loyalty")
public class LoyaltyResource extends AbstractResource {

    @Autowired
    LoyaltyService loyaltyService;

    @Autowired
    CoffeeStampService coffeeStampService;

    @Autowired
    private ApplicationConfig applicationConfig;

    @GET
    @Path("/add")
    public Response addLoyalty(@QueryParam("recieptNumber") String recieptNumber, @QueryParam("userId") String userId) {

        try {

            if (Boolean.parseBoolean(applicationConfig.getDebug()))  {
                return Response.ok().entity(loyaltyService.debugAddLoyalty(Integer.valueOf(userId))).build();
            }

            return Response.ok().entity(loyaltyService.createLoyaltyStamp(recieptNumber, userId)).build();
        } catch (Exception e) {
            throw NewExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, LoyaltyResource.class, "addLoyalty", null);
        }
    }

    @GET
    @Path("/claim")
    public Response claimLoyalty(@QueryParam("userId") String userId) {

        try {
            return Response.ok().entity(coffeeStampService.claimLoyalty(userId)).build();
        } catch (Exception e) {
            throw NewExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, LoyaltyResource.class, "claimLoyalty", null);
        }
    }

    @GET
    @Path("/history")
    public Response getLoyaltyHistory(@QueryParam("userId") String userId) {

        try {
            return Response.ok().entity(loyaltyService.getLoyaltyHistory(Integer.parseInt(userId))).build();
        } catch (Exception e) {
            throw NewExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, LoyaltyResource.class, "getLoyaltyHistory", null);
        }
    }
}

