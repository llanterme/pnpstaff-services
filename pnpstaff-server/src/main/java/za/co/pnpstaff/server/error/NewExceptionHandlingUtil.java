package za.co.pnpstaff.server.error;


import org.apache.commons.lang3.exception.ExceptionUtils;
import za.co.pnpstaff.core.exception.*;

import javax.ws.rs.core.Response;

public class NewExceptionHandlingUtil {

    private static NewExceptionHandlingUtil instance = new NewExceptionHandlingUtil();

    public static NewExceptionHandlingUtil get() {
        return instance;
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public CustomWebApplicationException handleAndGetExceptionFromResource(Exception e, Class<?> resource,
                                                                           String method, Object contextObject) {

        CustomWebApplicationException mappedException = e instanceof CustomWebApplicationException ? (CustomWebApplicationException) e : mapException(e);

        return mappedException;
    }


    protected CustomWebApplicationException mapException(Exception e) {

        if (e instanceof ApiNotConfiguredException) {

            return new CustomWebApplicationException(Response.Status.NOT_FOUND,
                    getErrorMessage("001", "Api not configured.", e), e);
        }

        if (e instanceof TillRecieptInvalidException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("002", "Your purchase reciept seems invalid.", e), e);
        }

        if (e instanceof ReceiptAlreadyCapturedException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("002", "You have already claimed this purchase.", e), e);
        }

        if (e instanceof BasketDoesntQualifyException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("003", "Your purchase doesnt qualify.", e), e);
        }

        if (e instanceof UnableToSaveStampException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("004", "Unable to save stamp.", e), e);
        }

        if (e instanceof UnableToSaveReceiptException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("005", "Unable to save receipt.", e), e);
        }

        if (e instanceof UnableToFindUserException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("005", "Unable to find user.", e), e);
        }

        if (e instanceof UserStampsExceededException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("006", "You have a free coffee waiting. Claim it now!", e), e);
        }

        if (e instanceof TillReceiptTimeHasExpiredException) {

            return new CustomWebApplicationException(Response.Status.BAD_REQUEST,
                    getErrorMessage("007", "Reciept has expired.", e), e);
        }

        return new CustomWebApplicationException(Response.Status.INTERNAL_SERVER_ERROR,
                getErrorMessage("999", "Unexpected error.", e), e);

    }

    protected ErrorMessage getErrorMessage(String code, String message, Exception e) {
        return new ErrorMessage(code, message, ExceptionUtils.getStackTrace(e));
    }





}
