package za.co.pnpstaff.server.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.pnpstaff.core.service.interfaces.ApiService;
import za.co.pnpstaff.server.error.NewExceptionHandlingUtil;
import za.co.pnpstaff.server.security.JWTTokenNeeded;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/api")
public class ApiInfoResource extends AbstractResource {

    @Autowired
    ApiService apiService;

    @GET
    @Path("/version")
    @JWTTokenNeeded
    public Response getEventVersion() {

        try {
            return Response.ok().entity(apiService.getApiVersion()).build();
        } catch (Exception e) {
            throw NewExceptionHandlingUtil.get().handleAndGetExceptionFromResource(e, ApiInfoResource.class, "version", null);
        }
    }
}
