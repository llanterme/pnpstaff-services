package za.co.pnpstaff.server.security;

/**
 * Created by Luke on 3/22/17.
 */

import java.security.Key;


public interface KeyGenerator {

    Key generateKey();
}
