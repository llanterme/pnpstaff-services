package za.co.pnpstaff.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import za.co.pnpstaff.core.ApplicationConfig;
import za.co.pnpstaff.core.domain.Basket;
import za.co.pnpstaff.core.domain.BasketQualifier;
import za.co.pnpstaff.core.domain.Receipt;
import za.co.pnpstaff.core.exception.ServiceException;
import za.co.pnpstaff.core.exception.TillRecieptInvalidException;
import za.co.pnpstaff.core.service.interfaces.BasketService;
import za.co.pnpstaff.infrastructure.dao.db2.entities.BasketItemsEntity;
import za.co.pnpstaff.infrastructure.dao.db2.repo.BasketItemRepo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

@Service
public class BasketServiceImpl implements BasketService {


    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    BasketItemRepo basketItemRepo;

    @Override
    public List<Basket> getBasket(String recieptNumber) throws ServiceException {

        try {
            Receipt recieptParts = splitRecieptNumber(recieptNumber);

            List<Basket> returnList = new ArrayList<>();
            for (BasketItemsEntity basket : basketItemRepo.getBasket(Integer.valueOf(recieptParts.getTransactionNumber()), Integer.valueOf(recieptParts.getTillId()))) {
                Basket aBasket = new Basket();
                aBasket.setCompanyCode(basket.getCompanyCode());
                aBasket.setSelcCode(basket.getSelcCode());
                aBasket.setDescription(basket.getDescription());
                returnList.add(aBasket);
            }

            return returnList;
        }
            catch(TillRecieptInvalidException e) {
                throw new TillRecieptInvalidException();
            }
            catch (ServiceException e) {
                throw new ServiceException();
            }
    }

    @Override
    public BasketQualifier doesBasketQualify(String recieptNumber) throws ServiceException {

        BasketQualifier basketQualifier = new BasketQualifier();

        try {
            Receipt recieptParts = splitRecieptNumber(recieptNumber);
            List<BasketItemsEntity> basket = basketItemRepo.getQualifyingBasket(Integer.valueOf(recieptParts.getTransactionNumber()), Integer.valueOf(recieptParts.getTillId()));

            //check if basket has a qualifier
            if(basket.size() != 0) {

                boolean qualiferDoestExist = false;
                for (String item : getProductQualifiers()) {
                    while (!qualiferDoestExist) {
                        if (containsName(basket, item)) {
                            basketQualifier.setQualifyingItem(true);
                            qualiferDoestExist = true;
                            break;
                        } else {
                            basketQualifier.setQualifyingItem(false);
                            break;
                        }

                    }
                }

                //has time expired to redeem?
                basketQualifier.setReceiptTimeExpired(hasReceiptExpired(basket.stream().findFirst().get().getTxnDate()));
            } else {
                basketQualifier.setQualifyingItem(false);
            }

            return basketQualifier;


        }
        catch(TillRecieptInvalidException e) {
            throw new TillRecieptInvalidException();
        }
        catch (Exception e) {
        throw new ServiceException();
        }


    }

    protected List<String> getProductQualifiers() {
        List<String> productQualifiers = new ArrayList<>();
        for (String i: applicationConfig.getQualifyingProducts().split("\\|")) {
            productQualifiers.add(i);
        }
        return productQualifiers;

    }

    protected boolean containsName(final List<BasketItemsEntity> list, final String name){
        return list.stream().filter(o -> o.getDescription().equals(name)).findFirst().isPresent();
    }

    protected boolean hasReceiptExpired(String txnDate) throws ServiceException {

        try {
            Date now = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSSZ");
            Date date1 = simpleDateFormat.parse(txnDate);

            long MAX_DURATION = MILLISECONDS.convert(applicationConfig.getReceiptExpireTime(), MINUTES);

            long duration = now.getTime() - (date1.getTime() - 7200000);

            if (duration > MAX_DURATION) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
        throw new ServiceException(e.getMessage());
        }

    }

    protected Receipt splitRecieptNumber (String reciept) throws ServiceException {

        if(reciept.length() != 20) {
            throw new TillRecieptInvalidException();

        }
        String recieptNumber = reciept.substring(10).replaceFirst(".$","");
        String tillNumber = recieptNumber.substring(0,3);
        String transactionNumber = recieptNumber.substring(3);

          return new Receipt(tillNumber, transactionNumber);


    }
}

