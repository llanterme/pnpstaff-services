package za.co.pnpstaff.core.service.interfaces;

import za.co.pnpstaff.core.domain.LoyaltyHistory;
import za.co.pnpstaff.core.domain.Loyalty;
import za.co.pnpstaff.core.exception.ServiceException;

import java.util.List;

/**
 * Created by Luke on 3/29/17.
 */
public interface LoyaltyService {

    Loyalty createLoyaltyStamp(String recieptNumber, String userId) throws ServiceException;

    List<LoyaltyHistory> getLoyaltyHistory(int userld) throws ServiceException;

    Loyalty debugAddLoyalty(int userId);

}
