package za.co.pnpstaff.core.domain;


public class CoffeeStamp {

    private int coffeeStampId;
    private int UserId;
    private int stampCount;
    private int runningCount;

    public int getCoffeeStampId() {
        return coffeeStampId;
    }

    public void setCoffeeStampId(int coffeeStampId) {
        this.coffeeStampId = coffeeStampId;
    }

     public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getStampCount() {
        return stampCount;
    }

    public void setStampCount(int stampCount) {
        this.stampCount = stampCount;
    }

    public int getRunningCount() {
        return runningCount;
    }

    public void setRunningCount(int runningCount) {
        this.runningCount = runningCount;
    }
}
