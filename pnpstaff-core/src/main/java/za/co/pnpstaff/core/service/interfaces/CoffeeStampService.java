package za.co.pnpstaff.core.service.interfaces;

import za.co.pnpstaff.core.domain.CoffeeStamp;
import za.co.pnpstaff.core.domain.Loyalty;
import za.co.pnpstaff.core.exception.ServiceException;


public interface CoffeeStampService {

    CoffeeStamp addorUpdate(CoffeeStamp coffeeStamp) throws ServiceException;

    CoffeeStamp getUserCurrentCounts(int userId) throws ServiceException;


    CoffeeStamp claimLoyalty(String userId) throws ServiceException;

}
