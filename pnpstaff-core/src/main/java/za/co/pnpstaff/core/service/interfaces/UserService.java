package za.co.pnpstaff.core.service.interfaces;


import za.co.pnpstaff.core.domain.Api;
import za.co.pnpstaff.core.domain.User;
import za.co.pnpstaff.core.exception.ServiceException;

public interface UserService {

    User addUser(User user) throws ServiceException;

}
