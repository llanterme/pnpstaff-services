package za.co.pnpstaff.core.domain;

public class LoyaltyHistory {

    private String dateClaimed;

    private int count;

    public LoyaltyHistory(String dateClaimed, int count) {
        this.dateClaimed = dateClaimed;
        this.count = count;
    }

    public String getDateClaimed() {
        return dateClaimed;
    }

    public void setDateClaimed(String dateClaimed) {
        this.dateClaimed = dateClaimed;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
