package za.co.pnpstaff.core.domain;

/**
 * Created by Luke on 3/28/17.
 */
public class Basket {

    private String companyCode;
    private String selcCode;
    private String description;


    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSelcCode() {
        return selcCode;
    }

    public void setSelcCode(String selcCode) {
        this.selcCode = selcCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
