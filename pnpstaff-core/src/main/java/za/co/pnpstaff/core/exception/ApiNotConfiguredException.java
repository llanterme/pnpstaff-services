package za.co.pnpstaff.core.exception;

/**
 * Created by Luke on 3/27/17.
 */
public class ApiNotConfiguredException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public ApiNotConfiguredException() {
    }

    public ApiNotConfiguredException(String message) {
        super(message);
    }

    public ApiNotConfiguredException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiNotConfiguredException(Throwable cause) {
        super(cause);
    }
}
