package za.co.pnpstaff.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.pnpstaff.core.domain.Api;
import za.co.pnpstaff.core.exception.ApiNotConfiguredException;
import za.co.pnpstaff.core.exception.ServiceException;
import za.co.pnpstaff.core.service.interfaces.ApiService;
import za.co.pnpstaff.infrastructure.dao.mysql.entities.ApiEntity;
import za.co.pnpstaff.infrastructure.dao.mysql.repo.ApiInfoRepository;

@Service
public class ApiServiceImpl implements ApiService {

    @Autowired
    ApiInfoRepository apiInfoRepository;

    @Override
    public Api getApiVersion() throws ServiceException {

        ApiEntity dbRawEntity = apiInfoRepository.findOne(1);

        if(dbRawEntity == null) {
            throw new ApiNotConfiguredException();
        }
        return new Api(dbRawEntity.getApiVersion());
    }
}
