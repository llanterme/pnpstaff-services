package za.co.pnpstaff.core.service.impl;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.pnpstaff.core.domain.CoffeeStamp;
import za.co.pnpstaff.core.exception.ServiceException;
import za.co.pnpstaff.core.exception.UnableToFindUserException;
import za.co.pnpstaff.core.exception.UnableToSaveStampException;
import za.co.pnpstaff.core.service.interfaces.CoffeeStampService;
import za.co.pnpstaff.infrastructure.dao.mysql.entities.CoffeeStampEntity;
import za.co.pnpstaff.infrastructure.dao.mysql.entities.UserEntity;
import za.co.pnpstaff.infrastructure.dao.mysql.repo.CoffeeStampRepository;


@Service
public class CoffeeStampServiceImpl implements CoffeeStampService {

    @Autowired
    CoffeeStampRepository coffeeStampRepository;

    @Override
    public CoffeeStamp addorUpdate(CoffeeStamp coffeeStamp) throws ServiceException {

        try {
            CoffeeStamp currentUserCounts = getUserCurrentCounts(coffeeStamp.getUserId());
            if (currentUserCounts != null) {

               return updateCoffeeStamp(currentUserCounts ,coffeeStamp.getRunningCount(),coffeeStamp.getStampCount());

            } else {
               return addCoffeeStamp(coffeeStamp);
            }
        } catch (UnableToFindUserException e) {
            throw new UnableToFindUserException(e);
        }
        catch (UnableToSaveStampException e) {
            throw  new UnableToSaveStampException();
        }
        catch (ServiceException e) {
            throw new ServiceException(e);
        }
    }


    public CoffeeStamp claimLoyalty(String userId) throws ServiceException {

        // get currentCounts
        CoffeeStamp userCounts = getUserCurrentCounts(Integer.valueOf(userId));
        userCounts.setRunningCount(userCounts.getRunningCount());
        userCounts.setStampCount(0);
        userCounts.setUserId(Integer.valueOf(userId));
        userCounts.setCoffeeStampId(userCounts.getCoffeeStampId());


        return updateCoffeeStamp(userCounts,0,0 );


    }


    public CoffeeStamp getUserCurrentCounts(int userId) throws ServiceException {

        CoffeeStampEntity user = coffeeStampRepository.userByuserId(userId);
        CoffeeStamp coffeeStamp = new CoffeeStamp();
        if(user != null) {

           coffeeStamp.setStampCount(user.getStampCount());
            coffeeStamp.setRunningCount(user.getRunningCount());
            coffeeStamp.setUserId(userId);
            coffeeStamp.setCoffeeStampId(user.getCoffeeStampId());

            return coffeeStamp;
        }

        return null;
    }

    protected CoffeeStamp updateCoffeeStamp(CoffeeStamp currentUserCounts, int runningCount, int stampCount) throws ServiceException {

        try {

                CoffeeStampEntity entry = new CoffeeStampEntity();
                entry.setUserId(currentUserCounts.getUserId());
                entry.setRunningCount(currentUserCounts.getRunningCount() + runningCount);
                entry.setStampCount(currentUserCounts.getStampCount() + stampCount);
                entry.setCoffeeStampId(currentUserCounts.getCoffeeStampId());

                CoffeeStampEntity newDbEntry = coffeeStampRepository.saveAndFlush(entry);

                if (newDbEntry == null) {
                    throw new UnableToSaveStampException();
                }

                final CoffeeStamp coffeeStamp = new CoffeeStamp();
                coffeeStamp.setCoffeeStampId(newDbEntry.getCoffeeStampId());
                coffeeStamp.setStampCount(newDbEntry.getStampCount());
                coffeeStamp.setRunningCount(newDbEntry.getRunningCount());
                coffeeStamp.setUserId(newDbEntry.getUserId());

                return coffeeStamp;
        }
     catch (ServiceException e) {
        throw new ServiceException();
    }

    }

    protected CoffeeStamp addCoffeeStamp(CoffeeStamp coffeeStamp) throws ServiceException {

        try {

            CoffeeStampEntity entry = new CoffeeStampEntity();

                entry.setUserId(coffeeStamp.getUserId());
                entry.setRunningCount(coffeeStamp.getRunningCount());
                entry.setStampCount(coffeeStamp.getStampCount());

            CoffeeStampEntity newDbEntry = coffeeStampRepository.saveAndFlush(entry);

            if(newDbEntry == null) {
                throw new UnableToSaveStampException();
            }
            coffeeStamp.setCoffeeStampId(newDbEntry.getCoffeeStampId());

            return coffeeStamp;
        }
        catch (ServiceException e) {
            throw new ServiceException();
        }

    }


}

