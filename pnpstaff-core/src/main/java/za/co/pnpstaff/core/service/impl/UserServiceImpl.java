package za.co.pnpstaff.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.pnpstaff.core.domain.User;
import za.co.pnpstaff.core.exception.ServiceException;
import za.co.pnpstaff.core.service.interfaces.UserService;
import za.co.pnpstaff.infrastructure.dao.mysql.entities.UserEntity;
import za.co.pnpstaff.infrastructure.dao.mysql.repo.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User addUser(User user) throws ServiceException {
        try {
            if(!doestExist(user)) {

                UserEntity newUser = new UserEntity();
                newUser.setDeviceId(user.getDeviceId());
                UserEntity createdUser = userRepository.saveAndFlush(newUser);

                user.setUserId(createdUser.getUserId());

                return user;
            }

        } catch (ServiceException e) {
            throw new ServiceException();
        }

        return null;
    }


    private boolean doestExist(User user) throws ServiceException {

        return userRepository.userAlreadyCaptured(user.getDeviceId());
  }
}
