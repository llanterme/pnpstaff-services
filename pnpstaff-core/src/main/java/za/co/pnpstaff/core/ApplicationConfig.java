package za.co.pnpstaff.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application.properties")
@ConfigurationProperties(prefix = "app")
public class ApplicationConfig {

    @Value("${app.receiptExpireTime}")
    private int receiptExpireTime;

    @Value("${app.maxStampCount}")
    private int maxStampCount;

    @Value("${app.qualifyingProducts}")
    private String qualifyingProducts;

    @Value("${app.debug}")
    private String debug;


    public String getDebug() {
        return debug;
    }

    public void setDebug(String debug) {
        this.debug = debug;
    }

    public String getQualifyingProducts() {
        return qualifyingProducts;
    }

    public void setQualifyingProducts(String qualifyingProducts) {
        this.qualifyingProducts = qualifyingProducts;
    }

    public int getMaxStampCount() {
        return maxStampCount;
    }

    public void setMaxStampCount(int maxStampCount) {
        this.maxStampCount = maxStampCount;
    }

    public int getReceiptExpireTime() {
        return receiptExpireTime;
    }

    public void setReceiptExpireTime(int receiptExpireTime) {
        this.receiptExpireTime = receiptExpireTime;
    }



}
