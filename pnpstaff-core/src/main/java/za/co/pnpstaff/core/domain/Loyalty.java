package za.co.pnpstaff.core.domain;

/**
 * Created by Luke on 3/29/17.
 */
public class Loyalty {

    private String receiptNumber;
    private int userId;
    private CoffeeStamp coffeeStamp;

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public CoffeeStamp getCoffeeStamp() {
        return coffeeStamp;
    }

    public void setCoffeeStamp(CoffeeStamp coffeeStamp) {
        this.coffeeStamp = coffeeStamp;
    }
}
