package za.co.pnpstaff.core.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.pnpstaff.core.ApplicationConfig;
import za.co.pnpstaff.core.domain.BasketQualifier;
import za.co.pnpstaff.core.domain.LoyaltyHistory;
import za.co.pnpstaff.core.domain.CoffeeStamp;
import za.co.pnpstaff.core.domain.Loyalty;
import za.co.pnpstaff.core.exception.*;
import za.co.pnpstaff.core.service.interfaces.BasketService;
import za.co.pnpstaff.core.service.interfaces.CoffeeStampService;
import za.co.pnpstaff.core.service.interfaces.LoyaltyService;
import za.co.pnpstaff.infrastructure.dao.mysql.entities.LoyaltyEntity;
import za.co.pnpstaff.infrastructure.dao.mysql.repo.LoyaltyRepository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LoyaltyServiceImpl implements LoyaltyService {


    private static final Integer STAMP_COUNT = 1;
    private static final Integer STAMP_RUNNING_COUNT = 1;

    @Autowired
    LoyaltyRepository coffeeLoyaltyRepository;

    @Autowired
    BasketService basketService;

    @Autowired
    CoffeeStampService coffeeStampService;

    @Autowired
    private ApplicationConfig applicationConfig;


    @Override
    public List<LoyaltyHistory> getLoyaltyHistory(int userld) throws ServiceException {

        try {
            List<LoyaltyHistory> loyaltyHistoryList = new ArrayList<>();
            for (Object[] a : coffeeLoyaltyRepository.getLoyaltyHistory(userld)) {
                loyaltyHistoryList.add(new LoyaltyHistory(a[0].toString(), Integer.parseInt(a[1].toString())));
          }
            return loyaltyHistoryList;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }

    }

    public Loyalty createLoyaltyStamp(String recieptNumber, String userId) throws ServiceException {

        final Loyalty loyalty;
        BasketQualifier basketQualifier;

            // Check if till receipt already exists
            if (coffeeLoyaltyRepository.receiptAlreadyCaptured(recieptNumber)) {
                throw new ReceiptAlreadyCapturedException();
            } else {
                // Check if basket contains a qualifier
                basketQualifier = basketService.doesBasketQualify(recieptNumber);

                if(!basketQualifier.isQualifyingItem()) {
                    throw new BasketDoesntQualifyException();
                }

                // Time expired?
                if(basketQualifier.isReceiptTimeExpired()) {
                    throw new TillReceiptTimeHasExpiredException();
                }

                else {
                    loyalty = persistNewLoyaltyEntry(recieptNumber, userId);
                }
            }

            return loyalty;

    }

    protected Loyalty persistNewLoyaltyEntry(String receiptNumber, String userId) throws ServiceException {

        try {

            // get currentCounts
            CoffeeStamp userCounts = coffeeStampService.getUserCurrentCounts(Integer.valueOf(userId));


            if(userCounts != null) {
                if (userCounts.getStampCount() == applicationConfig.getMaxStampCount()) {
                    throw new UserStampsExceededException();
                }
            }

            Date curDate = new Date();

            SimpleDateFormat format;
            format = new SimpleDateFormat("MMM yyyy");
            String DateToStr = format.format(curDate);


            LoyaltyEntity newCoffeeEntry = new LoyaltyEntity();
            newCoffeeEntry.setReceiptNumber(receiptNumber);
            newCoffeeEntry.setDateRedeemed(DateToStr);
            newCoffeeEntry.setUserId(Integer.valueOf(userId));
            LoyaltyEntity newdbEntry = coffeeLoyaltyRepository.saveAndFlush(newCoffeeEntry);

            if (newdbEntry == null) {
            throw new UnableToSaveReceiptException();
         }

            Loyalty loyaltyGraphy = new Loyalty();
            loyaltyGraphy.setReceiptNumber(receiptNumber);
            loyaltyGraphy.setUserId(Integer.valueOf(userId));
            loyaltyGraphy.setCoffeeStamp(coffeeStampService.addorUpdate(createCoffeeStamp(Integer.valueOf(userId))));

             return loyaltyGraphy;

        }

        catch (UnableToSaveStampException e) {
            throw new UnableToSaveStampException();
        }


    }

    protected CoffeeStamp createCoffeeStamp(int userId) {

        CoffeeStamp coffeeStamp = new CoffeeStamp();
        coffeeStamp.setStampCount(STAMP_COUNT);
        coffeeStamp.setRunningCount(STAMP_RUNNING_COUNT);
        coffeeStamp.setUserId(userId);

        return coffeeStamp;
    }

    public Loyalty debugAddLoyalty(int userId) {

        try {
        Loyalty loyalty = new Loyalty();
        loyalty.setUserId(17);
        loyalty.setReceiptNumber("99920283746352617261");
        loyalty.setCoffeeStamp(coffeeStampService.getUserCurrentCounts(17));

        return loyalty;
        } catch (Exception e) {

        }
        return null;
    }

}
