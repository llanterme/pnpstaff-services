package za.co.pnpstaff.core.exception;

/**
 * Created by Luke on 3/27/17.
 */
public class UnableToSaveStampException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnableToSaveStampException() {
    }

    public UnableToSaveStampException(String message) {
        super(message);
    }

    public UnableToSaveStampException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToSaveStampException(Throwable cause) {
        super(cause);
    }
}
