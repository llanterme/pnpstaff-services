package za.co.pnpstaff.core.domain;


public class User {
    private int userId;
    private String deviceId;

    public User() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public User(int userId, String deviceId) {
        this.userId = userId;
        this.deviceId = deviceId;
    }
}

