package za.co.pnpstaff.core.service.interfaces;

import za.co.pnpstaff.core.domain.Basket;
import za.co.pnpstaff.core.domain.BasketQualifier;
import za.co.pnpstaff.core.exception.ServiceException;

import java.util.List;

public interface BasketService {
    List<Basket> getBasket(String recieptNumber) throws ServiceException;

    BasketQualifier doesBasketQualify(String recieptNumber) throws ServiceException;



}
