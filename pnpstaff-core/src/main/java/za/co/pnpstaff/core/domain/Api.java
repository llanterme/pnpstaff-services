package za.co.pnpstaff.core.domain;

public class Api {

    private String apiVersion;

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public Api(String apiVersion) {
        this.apiVersion = apiVersion;
    }
}
