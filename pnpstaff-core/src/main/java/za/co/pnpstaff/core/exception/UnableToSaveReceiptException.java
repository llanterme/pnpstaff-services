package za.co.pnpstaff.core.exception;

/**
 * Created by Luke on 3/27/17.
 */
public class UnableToSaveReceiptException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnableToSaveReceiptException() {
    }

    public UnableToSaveReceiptException(String message) {
        super(message);
    }

    public UnableToSaveReceiptException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToSaveReceiptException(Throwable cause) {
        super(cause);
    }
}
