package za.co.pnpstaff.core.exception;

/**
 * Created by Luke on 3/27/17.
 */
public class TillRecieptInvalidException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public TillRecieptInvalidException() {
    }

    public TillRecieptInvalidException(String message) {
        super(message);
    }

    public TillRecieptInvalidException(String message, Throwable cause) {
        super(message, cause);
    }

    public TillRecieptInvalidException(Throwable cause) {
        super(cause);
    }
}
