package za.co.pnpstaff.core.exception;

/**
 * Created by Luke on 3/27/17.
 */
public class UnableToFindUserException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public UnableToFindUserException() {
    }

    public UnableToFindUserException(String message) {
        super(message);
    }

    public UnableToFindUserException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToFindUserException(Throwable cause) {
        super(cause);
    }
}
