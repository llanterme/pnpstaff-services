package za.co.pnpstaff.core.service.interfaces;


import za.co.pnpstaff.core.domain.Api;
import za.co.pnpstaff.core.exception.ServiceException;

public interface ApiService {

    Api getApiVersion() throws ServiceException;
}
