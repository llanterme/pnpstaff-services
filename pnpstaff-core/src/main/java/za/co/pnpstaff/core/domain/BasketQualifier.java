package za.co.pnpstaff.core.domain;

/**
 * Created by Luke on 4/7/17.
 */
public class BasketQualifier {

    boolean qualifyingItem;

    boolean receiptTimeExpired;

    public boolean isQualifyingItem() {
        return qualifyingItem;
    }

    public void setQualifyingItem(boolean qualifyingItem) {
        this.qualifyingItem = qualifyingItem;
    }

    public boolean isReceiptTimeExpired() {
        return receiptTimeExpired;
    }

    public void setReceiptTimeExpired(boolean receiptTimeExpired) {
        this.receiptTimeExpired = receiptTimeExpired;
    }
}
