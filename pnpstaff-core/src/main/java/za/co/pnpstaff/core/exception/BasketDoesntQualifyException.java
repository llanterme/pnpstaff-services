package za.co.pnpstaff.core.exception;

/**
 * Created by Luke on 3/27/17.
 */
public class BasketDoesntQualifyException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public BasketDoesntQualifyException() {
    }

    public BasketDoesntQualifyException(String message) {
        super(message);
    }

    public BasketDoesntQualifyException(String message, Throwable cause) {
        super(message, cause);
    }

    public BasketDoesntQualifyException(Throwable cause) {
        super(cause);
    }
}
