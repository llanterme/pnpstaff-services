package za.co.pnpstaff.core.exception;

/**
 * Created by Luke on 3/27/17.
 */
public class TillReceiptTimeHasExpiredException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public TillReceiptTimeHasExpiredException() {
    }

    public TillReceiptTimeHasExpiredException(String message) {
        super(message);
    }

    public TillReceiptTimeHasExpiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public TillReceiptTimeHasExpiredException(Throwable cause) {
        super(cause);
    }
}
