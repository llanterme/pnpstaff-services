package za.co.pnpstaff.core.exception;

/**
 * Created by Luke on 3/27/17.
 */
public class ReceiptAlreadyCapturedException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = 1184268032847741356L;

    public ReceiptAlreadyCapturedException() {
    }

    public ReceiptAlreadyCapturedException(String message) {
        super(message);
    }

    public ReceiptAlreadyCapturedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReceiptAlreadyCapturedException(Throwable cause) {
        super(cause);
    }
}
