package za.co.pnpstaff.core.domain;

/**
 * Created by Luke on 3/28/17.
 */
public class Receipt {

    private String tillId;
    private String transactionNumber;
    private String receiptNumber;

    public Receipt(){}

    public String getTillId() {
        return tillId;
    }

    public void setTillId(String tillId) {
        this.tillId = tillId;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public Receipt(String tillId, String transactionNumber) {
        this.tillId = tillId;
        this.transactionNumber = transactionNumber;
    }
}
