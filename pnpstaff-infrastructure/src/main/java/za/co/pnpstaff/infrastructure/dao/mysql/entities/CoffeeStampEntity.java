package za.co.pnpstaff.infrastructure.dao.mysql.entities;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "coffee_stamp")
public class CoffeeStampEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="coffee_stamp_id")
    private int coffeeStampId;

    @Column(name="user_id")
    private int userId;

    @Column(name="stamp_count")
    private int stampCount;

    @Column(name="running_stamp_count")
    private int runningCount;

    public int getCoffeeStampId() {
        return coffeeStampId;
    }

    public void setCoffeeStampId(int coffeeStampId) {
        this.coffeeStampId = coffeeStampId;
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStampCount() {
        return stampCount;
    }

    public void setStampCount(int stampCount) {
        this.stampCount = stampCount;
    }

    public int getRunningCount() {
        return runningCount;
    }

    public void setRunningCount(int runningCount) {
        this.runningCount = runningCount;
    }
}
