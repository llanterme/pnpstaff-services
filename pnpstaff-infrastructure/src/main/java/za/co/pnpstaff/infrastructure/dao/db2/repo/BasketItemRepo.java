package za.co.pnpstaff.infrastructure.dao.db2.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.pnpstaff.infrastructure.dao.db2.entities.BasketItemsEntity;

import java.util.List;

@Repository
@Transactional
public interface BasketItemRepo extends JpaRepository<BasketItemsEntity, Integer> {

    @Query(value = "select basket.TXHD_TXN_NR, basket.TXDE_DETAIL_NR, basket.ORGU_CODE_CMPY, basket.SELC_CODE, items.SELC_DESC from PCMS_DM.TXN_DETAIL basket, PCMS_DM.SELLING_CODE items WHERE basket.SELC_CODE = items.SELC_CODE AND basket.TXHD_TXN_NR = :transactionNumber AND basket.TILL_SHORT_DESC = :tillId", nativeQuery = true)
    List<BasketItemsEntity> getBasket(@Param("transactionNumber") int transactionNumber, @Param("tillId") int tillId);

     @Query(value = "select basket.TXHD_TXN_NR, basket.TXDE_DETAIL_NR, basket.ORGU_CODE_CMPY, basket.SELC_CODE, items.SELC_DESC, header.TXHD_FINISH_DATE from PCMS_DM.TXN_DETAIL basket, PCMS_DM.SELLING_CODE items, PCMS_DM.TXN_HEADER header WHERE basket.SELC_CODE = items.SELC_CODE AND basket.TXHD_TXN_NR = :transactionNumber AND basket.TILL_SHORT_DESC = :tillId AND basket.TXHD_TXN_NR = header.TXHD_TXN_NR AND basket.TILL_SHORT_DESC = header.TILL_SHORT_DESC", nativeQuery = true)
    List<BasketItemsEntity> getQualifyingBasket(@Param("transactionNumber") int transactionNumber, @Param("tillId") int tillId);


}
