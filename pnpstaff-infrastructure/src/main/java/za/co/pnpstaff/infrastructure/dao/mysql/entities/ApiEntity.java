package za.co.pnpstaff.infrastructure.dao.mysql.entities;

import javax.persistence.*;

@Entity
@Table(name = "api_info")
public class ApiEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="api_info_id")
    private int apiInfoId;

    @Column(name="api_version")
    private String apiVersion;


    public int getApiInfoId() {
        return apiInfoId;
    }

    public void setApiInfoId(int apiInfoId) {
        this.apiInfoId = apiInfoId;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }
}
