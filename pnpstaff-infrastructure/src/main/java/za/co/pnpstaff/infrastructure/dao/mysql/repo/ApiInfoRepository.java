package za.co.pnpstaff.infrastructure.dao.mysql.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.pnpstaff.infrastructure.dao.mysql.entities.ApiEntity;

@Repository
@Transactional
public interface ApiInfoRepository extends JpaRepository<ApiEntity, Integer> {

}
