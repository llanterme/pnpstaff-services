package za.co.pnpstaff.infrastructure.dao.mysql.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.pnpstaff.infrastructure.dao.mysql.entities.LoyaltyEntity;
import za.co.pnpstaff.infrastructure.dao.mysql.entities.UserEntity;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM UserEntity c WHERE c.deviceId = :deviceId")
    boolean userAlreadyCaptured(@Param("deviceId") String deviceId);


}
