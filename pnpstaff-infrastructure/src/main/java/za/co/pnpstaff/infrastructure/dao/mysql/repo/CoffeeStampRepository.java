package za.co.pnpstaff.infrastructure.dao.mysql.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.pnpstaff.infrastructure.dao.mysql.entities.CoffeeStampEntity;

@Repository
@Transactional
public interface CoffeeStampRepository extends JpaRepository<CoffeeStampEntity, Integer> {

    @Query("SELECT p FROM CoffeeStampEntity p WHERE p.userId = :userId")
    CoffeeStampEntity userByuserId(@Param("userId") int userId);

}
