package za.co.pnpstaff.infrastructure.dao.mysql.entities;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "coffee_loyalty")
public class LoyaltyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="coffee_loyalty_id")
    private int coffeeLoyaltyId;

    @Column(name="receipt_number")
    private String receiptNumber;

    @Column(name="date_redeemed")
    private String dateRedeemed;


    @Column(name="user_id")
    private int userId;

    public int getCoffeeLoyaltyId() {
        return coffeeLoyaltyId;
    }

    public void setCoffeeLoyaltyId(int coffeeLoyaltyId) {
        this.coffeeLoyaltyId = coffeeLoyaltyId;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getDateRedeemed() {
        return dateRedeemed;
    }

    public void setDateRedeemed(String dateRedeemed) {
        this.dateRedeemed = dateRedeemed;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
