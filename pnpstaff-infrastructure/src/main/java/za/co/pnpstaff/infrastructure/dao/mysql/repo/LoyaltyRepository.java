package za.co.pnpstaff.infrastructure.dao.mysql.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import za.co.pnpstaff.infrastructure.dao.mysql.entities.LoyaltyEntity;

import java.util.List;

@Repository
@Transactional
public interface LoyaltyRepository extends JpaRepository<LoyaltyEntity, Integer> {

    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM LoyaltyEntity c WHERE c.receiptNumber = :receiptNumber")
    boolean receiptAlreadyCaptured(@Param("receiptNumber") String receiptNumber);

    @Query(value = "select date_redeemed, count(receipt_number) from coffee_loyalty where user_id= :userId group by date_redeemed", nativeQuery = true)
    List<Object[]> getLoyaltyHistory(@Param("userId") int userId);

}
