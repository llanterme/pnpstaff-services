package za.co.pnpstaff.infrastructure.dao.db2.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "TXN_DETAIL")
@SecondaryTable(name = "SELLING_CODE")
public class BasketItemsEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="TXDE_DETAIL_NR")
    private int detailNnumber;

    @Column(name="ORGU_CODE_CMPY")
    private String companyCode;

    @Column(name="SELC_CODE")
    private String selcCode;

    @Column(name="TXHD_TXN_NR")
    private String txnNumber;

    @Column(name="SELC_DESC")
    private String description;

    @Column(name="TXHD_FINISH_DATE")
    private String txnDate;

    public int getDetailNnumber() {
        return detailNnumber;
    }

    public void setDetailNnumber(int detailNnumber) {
        this.detailNnumber = detailNnumber;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSelcCode() {
        return selcCode;
    }

    public void setSelcCode(String selcCode) {
        this.selcCode = selcCode;
    }

    public String getTxnNumber() {
        return txnNumber;
    }

    public void setTxnNumber(String txnNumber) {
        this.txnNumber = txnNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }
}
